import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import moment from "moment";

/**
 * Generated class for the ShowNewsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-show-news",
  templateUrl: "show-news.html"
})
export class ShowNewsPage {
  newsData: any;
  publishDate: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.newsData = this.navParams.data.news;
    this.publishDate = moment(this.newsData.publishedAt).format(
      "MMMM Do YYYY, h:mm:ss a"
    );
  }

  ionViewDidLoad() {}
}
