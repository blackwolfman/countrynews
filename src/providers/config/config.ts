import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

/*
  Generated class for the ConfigProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ConfigProvider {
  newsApiUrl = "https://newsapi.org/v2/top-headlines?country=";
  apiKey = "56a897f12de243239f062b137f247f49";
  countryApi = "https://restcountries.eu/rest/v2/all";
  newsApiOpt2 = "https://api.newsriver.io/v2/search?query=";
  newsApiOpt2ApiKey =
    "sBBqsGXiYgF0Db5OV5tAw6hcBk-XP4wVUoJOZA__XQGsn99FvZyeB8pP9N4EyBttn2pHZrSf1gT2PUujH1YaQA";
  //https://api.newsriver.io/v2/search?query=haiti&sortBy=_score&sortOrder=DESC&limit=15

  constructor(public http: HttpClient) {}
}
