import { BrowserModule } from "@angular/platform-browser";
import { ErrorHandler, NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";

import { MyApp } from "./app.component";
import { HomePage } from "../pages/home/home";
import { NewsPage } from "../pages/news/news";
import { ShowNewsPage } from "../pages/show-news/show-news";

import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { ApiCountryProvider } from "../providers/api-country/api-country";
import { ConfigProvider } from "../providers/config/config";
import { HttpClientModule } from "@angular/common/http";

@NgModule({
  declarations: [MyApp, HomePage, NewsPage, ShowNewsPage],
  imports: [BrowserModule, IonicModule.forRoot(MyApp), HttpClientModule],
  bootstrap: [IonicApp],
  entryComponents: [MyApp, HomePage, NewsPage, ShowNewsPage],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ApiCountryProvider,
    ConfigProvider
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {}
