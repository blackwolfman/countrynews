import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { ApiCountryProvider } from "../../providers/api-country/api-country";
import { ShowNewsPage } from "../show-news/show-news";
import moment from "moment";
/**
 * Generated class for the NewsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-news",
  templateUrl: "news.html"
})
export class NewsPage {
  countryIso2: string;
  countryName: string;
  countryFlag: string;
  newsList: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private _serviceCount: ApiCountryProvider
  ) {
    this.countryIso2 = this.navParams.data.countryIso2;
    this.countryFlag = this.navParams.data.countryFlag;
    this.countryName = this.navParams.data.countryName;
    this._serviceCount
      .getNewsByCountry(this.countryIso2)
      .then(resp => {
        if ((<any>resp).flag) {
          this.newsList = (<any>resp).newsList;
        } else {
        }
      })
      .then(() => {
        if (this.newsList.length <= 0) {
          this._serviceCount.getNewsFrOpt2(this.countryName).then(resp => {
            let stringArr: Array<any> = [];
            let author: any;
            if ((<any>resp).flag) {
              (<any>resp).newsList.forEach((value, i) => {
                let urlImage: string;
                if (value.elements.length >= 1) {
                  urlImage = value.elements[0].url;
                }
                if (value.website != undefined) {
                  author = value.website.name;
                }

                console.log(urlImage);
                let newObject = {
                  author: author,
                  content: value.text,
                  description: value.title,
                  publishedAt: value.publishDate,
                  urlToImage: urlImage,
                  source: { name: value.title }
                };
                stringArr.push(newObject);
              });
            }

            this.newsList = stringArr;
          });
        }
      });
  }

  ionViewDidLoad() {}

  showNews(index) {
    let news = this.newsList[index];
    this.navCtrl.push(ShowNewsPage, { news: news });
  }
}
