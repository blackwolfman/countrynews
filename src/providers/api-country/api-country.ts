import {
  HttpClient,
  HttpHeaders,
  HttpInterceptor,
  HttpHandler,
  HttpRequest
} from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ConfigProvider } from "../config/config";
import {} from "@angular/common/http";

/*
  Generated class for the ApiCountryProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiCountryProvider {
  constructor(public http: HttpClient, private _configServ: ConfigProvider) {}

  public getCountryList() {
    let resp = { flag: false };
    return new Promise((resolve, reject) => {
      return this.http
        .get(this._configServ.countryApi)
        .toPromise()
        .then(
          resp => {
            if ((<any>resp).length > 0) {
              resp = { flag: true, countryList: resp };
              resolve(resp);
            } else {
              resolve(resp);
            }
          },
          err => {
            resolve(resp);
          }
        )
        .catch(exception => {
          resolve(resp);
        });
    });
  }

  public getNewsByCountry(country) {
    let countAndKey = country + "&" + "apikey=" + this._configServ.apiKey;
    let resp = { flag: false };
    return new Promise((resolve, reject) => {
      return this.http
        .get(this._configServ.newsApiUrl + countAndKey)
        .toPromise()
        .then(
          resp => {
            if ((<any>resp).status === "ok") {
              resp = { flag: true, newsList: (<any>resp).articles };
              resolve(resp);
            } else {
              resolve(resp);
            }
          },
          err => {
            resolve(resp);
          }
        )
        .catch(exception => {
          resolve(resp);
        });
    });
  }

  getNewsFrOpt2(country) {
    let resp = { flag: false };

    let countAndKey = country + "&sortBy=_score&sortOrder=DESC&limit=15 ";
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: this._configServ.newsApiOpt2ApiKey
      })
    };
    console.log(this._configServ.newsApiOpt2 + countAndKey);
    return new Promise((resolve, reject) => {
      return this.http
        .get(this._configServ.newsApiOpt2 + countAndKey, httpOptions)
        .toPromise()
        .then(
          resp => {
            resp = { flag: true, newsList: resp };
            resolve(resp);
          },
          err => {
            resolve(resp);
          }
        )
        .catch(exception => {
          console.log(exception);
          resolve(resp);
        });
    });
  }
}
