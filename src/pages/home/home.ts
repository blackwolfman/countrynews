import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { NewsPage } from "../news/news";
import { ApiCountryProvider } from "../../providers/api-country/api-country";
import { SplashScreen } from "@ionic-native/splash-screen";

@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage {
  countryList: any[];
  searchItem: any;
  defaultCList: any;
  constructor(
    public navCtrl: NavController,
    private _serviceCount: ApiCountryProvider,
    private splashScreen: SplashScreen
  ) {
    //this.splashScreen.show();
    this._serviceCount.getCountryList().then(resp => {
      if ((<any>resp).flag) {
        this.countryList = (<any>resp).countryList;
        this.defaultCList = this.countryList;
      }
    });
  }

  ionViewDidLoad() {
    this.splashScreen.hide();
  }

  searchByName(searchValue) {
    this.countryList = this.defaultCList;
    let countListSearch = this.countryList;
    if (searchValue.length >= 1) {
      searchValue = searchValue[0].toUpperCase() + searchValue.slice(1);
      const result = countListSearch.filter(country =>
        country.name.includes(searchValue)
      );
      this.countryList = result;
    } else {
      this.countryList = this.defaultCList;
    }
  }

  newsByCountryIso(countryIso2, countryFlag, countryName) {
    this.navCtrl.push(NewsPage, {
      countryIso2: countryIso2,
      countryFlag: countryFlag,
      countryName: countryName
    });
  }
}
